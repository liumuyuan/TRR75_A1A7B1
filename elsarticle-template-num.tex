%% 
%% Copyright 2007, 2008, 2009 Elsevier Ltd
%% 
%% This file is part of the 'Elsarticle Bundle'.
%% ---------------------------------------------
%% 
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%% 
%% The list of all files belonging to the 'Elsarticle Bundle' is
%% given in the file `manifest.txt'.
%% 

%% Template article for Elsevier's document class `elsarticle'
%% with numbered style bibliographic references
%% SP 2008/03/01

\documentclass[preprint,12pt]{elsarticle}

%% Use the option review to obtain double line spacing
%% \documentclass[authoryear,preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times]{elsarticle}
%% \documentclass[final,1p,times,twocolumn]{elsarticle}
%% \documentclass[final,3p,times]{elsarticle}
%% \documentclass[final,3p,times,twocolumn]{elsarticle}
%% \documentclass[final,5p,times]{elsarticle}
%% \documentclass[final,5p,times,twocolumn]{elsarticle}

%% For including figures, graphicx.sty has been loaded in
%% elsarticle.cls. If you prefer to use the old commands
%% please give \usepackage{epsfig}

%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{overpic}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{color}
\usepackage{subfigure}
\def\todo#1{{\color{red}{\textbf{TODO: #1}}}} % todo comments
%% The amsthm package provides extended theorem environments
%% \usepackage{amsthm}

%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers.
%% \usepackage{lineno}

\journal{Computer and Fluids}

\begin{document}

\begin{frontmatter}

%% Title, authors and addresses

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for theassociated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for theassociated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for theassociated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}

\title{Numerical Simulation of Binary Droplet Collisions at High Weber Numbers: high grid resolution, visualization... }

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \address[label1]{}
%% \address[label2]{}

\author{M. Liu, M. Reizler, G. Karch, S. Boblest, F. Sadlo, T. Ertl, (B. Weigand), K. Schulte, D. Bothe}

\address{}

\begin{abstract}
%% Text of abstract
Numerical simulations ...
\end{abstract}

\begin{keyword}

Droplet Collision \sep VOF 
%% keywords here, in the form: keyword \sep keyword

%% PACS codes here, in the form: \PACS code \sep code

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)

\end{keyword}

\end{frontmatter}

%% \linenumbers

%% main text
\section{Introduction}
\label{sec:intro}

Ejection of droplets from an unstable rim connected with a liquid film is frequently observed in many natural and industrial processes, such as in heavy rains hitting the ground and in impacts of fuel spray on the piston within a combustion chamber. The mechanism of the rim instability is of primary importance, since it determines directly the size distribution of the ejected secondary droplets.

Reviewing the previous publications, the studies of the rim instability were conducted mainly based on three contexts: (1), impact of a droplet on a solid substrate \cite{mehdizadeh2004}. (2), impact of a droplet on a liquid film \cite{cossali1997impact}. (3), binary droplet collision \cite{pan2009binary}. Some researchers tried to establish general models describing the rim instability \cite{roisman2010instability}\cite{agbaglah2013longitudinal}. Although the mechanism of the rim instability has been discussed for more than half a century, from the perspective of the authors of the present paper, Zhang et al. \cite{zhang2010wavelength} first provided compelling evidence for the statement that the Plateau-Rayleigh (PR) instability pattern dominates rim instability. In their work focusing on the impact of a droplet on a liquid film, they extracted the spectrum of instability from the images picturing instability grow on the rim obtained from the experiments. It is reported that for a range of parameters regime the peak position and width of the spectrum are in excellent agreement with that are predicted by the Rayleigh-Plateau mechanism. Following the idea of Zhang et al. \cite{zhang2010wavelength}, Liu and Bothe \cite{liu2016numerical} conducted numerical simulations of binary droplet collisions in the high Weber number regime, which is characterized by the presence of the unstable rim, and showed that the instability growing on the rim of the collision complex can also be predicted by the PR instability theory. In addition by applying the initial instability spectrum measured on the computed geometry of the collision complex into the theoretical theory for the prediction, the peak values of the maximum amplifications are also well predicted. In a simultaneously running study in the context of binary droplet collision, the authors of this paper find that the validity of the prediction of the instability growth by means of the PR theory is independent of the Reynolds number of the collision; Reynolds number is defined as $Re=\rho U_r D_0/\eta$, where $\rho$ is the droplet density, $U_r$ the relative velocity of the collision, $D_0$ the initial diameter of the droplets and $\eta$ the dynamic viscosity of the droplets. The authors would like to recommend the validation of the dominance of the PR instability theory for the rim instability in the context of droplet collision on a solid substrate.

It should be noted that the PR instability theory is a linear theory and it does not apply when the amplitude of a wavy perturbation is comparable with the wave length. An important part of this study is to unveil the characteristics of the flow in the phase when the linear theory is not valid anymore. While the simulation results of Liu and Bothe \cite{liu2016numerical} in the case of $We=442.3$ were in excellent agreement with corresponding experiments, the secondary droplets are earlier ejected from the rim compared to the experiment in the case of $We=802$; $We$ denotes the Weber number defined as $We=\rho U_r D_0^2/\sigma$, where $sigma$ is the surface tension. We re-conduct this simulation ($We=802$) with three relative grid resolutions to verify whether the insufficient gird resolution leads to this discrepancy. The number of used cells is as high as 1 billion in the finest resolution. The characteristics of the flow are evaluated based on the simulation results with the highest grid resolution, described in section \ref{sec:}. The governing equation and the numerical methods are described in section \ref{sec}. In addition, the study of the origin of the secondary droplets and the associated visualization methods are described in \ref{sec:}. 





%On the other hand, the rim instability can be described only when a toroidal rim emerges by definition. Thus, it is convenience to study the rim instability by dividing the development of the rim instability in to three sequential phase, described in detail in section \ref{sec:}.

%% The Appendices part is started with the command \appendix;
%% appendix sections are then done as normal sections
%% \appendix

%% \section{}
%% \label{}

%% If you have bibdatabase file and want bibtex to generate the
%% bibitems, please use
%%
%%  \bibliographystyle{elsarticle-num} 
%%  \bibliography{<your bibdatabase>}

%% else use the following coding to input the bibitems directly in the
%% TeX file.
\section{Governing equations}
\section{Numerical methods}


FS3D ...


In binary droplet collisions, a liquid lamella, i.e. a liquid film connecting the rim, emerges on the collision plane. This lamella is so thin, especially at high energetic collisions, that it ruptures in standard VOF simulations conducted with even relatively high grid resolutions due to an unwanted interference while computing the surface tension force from both sides of the lamella, which leads to strong deviation in the subsequent deformation of the collision complex. In order to obtain sound collisions dynamics, we employ a lamella stabilization method that prevents the artificial lamella rapture; refer to Liu and Bothe \cite{liu2016numerical} for a detailed description of the lamella rupture problem and the algorithm of the lamella stabilization method.

The simulation is initialized with a water droplet colliding with its mirror image, which is realized by using a symmetry plane. On the other boundaries, the homogeneous Neumann boundary condition for the velocity and zero pressure are prescribed. The cuboid domain is decomposed into equidistant cubic cells. The domain size is $10\times10\times1.25 \textnormal{mm}^{3}$. Three meshes $256\times256\times 64$, $512\times512\times 128$, $1028\times1028\times 256$ are used to study the role of the grid resolution. The initial droplet diameter is $0.7 \textnormal{mm}$. The Weber number and Reynolds number are $We=805.2$ and $Re=6370$, respectively. The environmental gas is air. The physical parameters of the collision correspond to that of the experimental work conducted by Pan et al. \cite{pan2009binary}.

In order to trigger the instability growing on the rim, a small white noise disturbance is exerted to the initial velocity field. The strength of the disturbance is characterized by the standard deviation of the Gaussian distribution of the added white noise, which is set to $1\%$ of the initial relative velocity of the flying droplets $U_r$. 


\section{Simulation results}

\begin{figure}
\centering
\includegraphics[width=1\textwidth,natwidth=2254,natheight=1481]{figures/Figure_9.png}
 \caption{Photographs and visualized simulation results of the head-on collision of water droplets with $We=805.2$, $Re=6370.0$. (a) experimental result of Pan et al. \cite{pan2009binary}. (b) simulation results without initial disturbance. (c) simulation result with an initial disturbance of $\Delta U =1\%$. The post-processing in the simulations is conducted with POV-Ray.} 
\label{fig:we805}
\end{figure}
\subsection{z.B. more general }

\subsection{z.B. more about the nonlinear unstable phase}

A more detailed interface evolution history for the case of $We=442.3$ with an initial disturbance of $\Delta U=1 \%$ is shown in Figure \ref{fig:splashing_shapes_more}. In these images, the phenomena in the nonlinear unstable phase, i.e.\ the growth of fingers and the detachment of secondary droplets, are illustrated more clearly. In order to gain more insights into the flow in the collision complex, the pressure field and the velocity field for a truncated area (originally in the black box) of Figure \ref{fig:splashing_shapes_more} are presented in Figure \ref{fig:splashing_p_u}. 

A negative pressure region in a curve form due to the negative interface curvature located at the joint of the sheet and the rim is observed in all of the presented pictures; in the same region the velocity is high. It seems that the fluid is drawn into the rim by the negative pressure region at least at the later stage of the deformation. The velocity fields show that the flow in the circumferential direction occurs only inside the rim (not considering the gas flow).

In the later phase of collision (nonlinear phase regarding the rim instability), it is impossible to predict the strength of the rim instability by means of the PR instability theory as described in section \ref{chap:pr_instability} due to the large deformation. Despite of this, instability patterns similar to the PR instability pattern are observed. Comparing the pressure field at $t^*=5.09$ and at $t^*=6.36$, the high pressure within the rim fraction between the middle finger and the right finger forces the fluid to flow out of the contracted rim fraction, which results in a rim fraction with a smaller diameter and even higher pressure within this rim fraction. The higher pressure in return forces more fluid to flow out of this rim fraction resulting in capillary pinching, which represents locally an unstable process similar to the PR instability pattern. This instability expels liquid out of the rim constantly and leads to the growth of the right finger and merging of the middle finger and the left finger. At $t^*=8.9$, the high pressure in the neck of the right finger forces the fluid to flow out of the neck constantly, which is also similar to the PR instability pattern, leading to a detachment of the secondary droplet at $t^*=9.67$.  

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{figures/splashing_442_shapes.jpg}
 \caption{Top view of interface evolution in the case $We=442.3$ with initial disturbance of $\Delta U=1\%$. The same simulation as shown in Figure \ref{fig:we442} (c).} 
 \label{fig:splashing_shapes_more}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.75\textwidth]{figures/splashing_442_p_u.jpg}
 \caption{Pressure field and velocity field of case $We = 442.3$ with initial disturbance of
$\Delta U = 1\%$. The same simulation as shown in Figure \ref{fig:we442} (c).} 
 \label{fig:splashing_p_u}
\end{figure}

\subsection{Visualization of the origin of the ejected droplets}

\begin{figure}[t]
\centering
\subfigure[$1.5\times 10^{-4}\,\textrm{s}$]{\includegraphics[width=0.49\linewidth]{figures/res-sep-24}\label{fig:vis-1}}
\subfigure[$1.8\times 10^{-4}\,\textrm{s}$]{\includegraphics[width=0.49\linewidth]{figures/res-sep-26}\label{fig:vis-2}}
\subfigure[$2.1\times 10^{-4}\,\textrm{s}$]{\includegraphics[width=0.49\linewidth]{figures/res-sep-25}\label{fig:vis-3}}
\subfigure[$5.4\times 10^{-4}\,\textrm{s}$]{\begin{overpic}[width=0.49\linewidth]{figures/res-sep-26-components}
  \put(0,100){\color{red}\line(1,0){52}}
  \put(0,100){\color{red}\line(0,-1){52}}
  \put(52,100){\color{red}\line(0,-1){52}}
  \put(0,48){\color{red}\line(1,0){52}}  

  \put(48,100){\color{red}\line(0,-1){48}}
  \put(0,52){\color{red}\line(1,0){48}}  

  \put(43,100){\color{red}\line(0,-1){43}}
  \put(0,57){\color{red}\line(1,0){43}}  
\end{overpic}\label{fig:vis-4}}
\caption{
Visualization of droplet segmentation.
\subref{fig:vis-1}--\subref{fig:vis-3}~Droplet before breakup is shown. 
In the visualization, its volume is partitioned such that each segment corresponds to different satellite droplet after breakup.
Notice the irregular pattern, and overlapping segments in the middle.
\subref{fig:vis-4}~Droplet after breakup, with colors of satellite droplet corresponding to the segments in the other figures.
The red boxes mark the relative sizes of figures~\subref{fig:vis-1}--\subref{fig:vis-3}. 
}\label{fig:vis}
\end{figure}
%
To investigate the formation of the satellite droplets, we employed a visualization technique that reveals the volumes belonging to these droplets already within the original droplet at the earlier simulation time steps before the droplets separate.
We find volumetric correspondences between the initial droplet and the satellite droplets resulting from breakup which leads to a segmentation of the initial droplet volume.
For visualization, meshes are constructed at the boundaries of the volume segments, such that each segment is surrounded by a closed mesh.

To find the correspondence between volume at the initial time step $t_0$ and droplets at the final time step $t_F$, we employ particle-based tracking.
Namely, at time $t_0$, particles are seeded at the centers of cells containing liquid phase, i.e., where $f>0$.
The particles are then advected using fourth-order Runge-Kutta method up to time $t_F$.
At this simulation time step, connected components algorithm is used to assign a unique label to each droplet.
The advected particles obtain the label of the surrounding droplet.
Afterwards, the assigned labels are transferred to the particle seed positions from time step $t_0$.

For the generation of boundary meshes, we consider each label separately.
Given a label $l$, a rectilinear grid is constructed that contains all seed points with this label.
The grid is positioned such that its nodes are aligned with the seed positions (i.e., with the center points of the simulation cells).
The grid points of the subgrid obtain value $1$ if a seed point with the label $l$ is present at the given position, or $0$ otherwise.
Afterwards, standard marching cubes~\cite{Lorensen:1987:MCH:37402.37422} algorithm is run that extracts the boundaries for the given label.
Because marching cubes interpolates the data on the edges of the mesh, the extracted mesh always passes through the edge midpoints if the edge is between a labeled and an empty node.

The visualization has been implemented as ParaView~\cite{ParaViewGuide} plugin that provides the visualized boundary as polygonal meshes  and the connected component labeling as a rectilinear grid of the same structure as the input data.
The data is visualized using standard ParaView processing tools, such as smoothing for more appealing display of the geometry.

Our visualization for the simulated dataset is shown in Figure~\ref{fig:vis}, where in \subref{fig:vis-1}--\subref{fig:vis-3}~the segmented droplet before breakup is visualized for the consecutive time steps (time in the captions indicates the initial time $t_0$), and in~\subref{fig:vis-4}---the droplet after breakup is shown together with the satellite droplets (time in the caption is the final time $t_F$).
Notice high irregularity of the segments, which are unevenly spaced along the perimeter, and have different radial extent.
Some of the segments occupy overlapping regions (e.g., the green segments at the left border). 
The resulting droplets, however, are distant from each other in Figure~\ref{fig:vis-4}.
Interestingly, the larger droplets stem from both broader and deeper regions.

\subsection*{Parallelization}
Parallelization was necessary for the visualization of the investigated simulation due to the large memory requirements of the data that could not be handled by a regular desktop computer.
We employ data parallelism (i.e., the approach adopted by ParaView), where data is split among several processes (possibly running on different machines), and each process works on a preassigned subdomain.
In this approach, explicit handling of communication between processes is necessary at several stages.

During particle advection, particles that leave a subdomain managed by a given process must be sent to appropriate  neighbor process.
To find out which one is responsible for the subdomain the particles have entered, a list of all processes together with the extent of the corresponding subdomain is kept in the memory of each process.
This way, the relevant process can be found by checking if a particle is contained within the respective extent.

Another global algorithm employed in our technique is the connected component labeling.
The implementation is based on the method proposed by Harrison et al.~\cite{EGPGV:EGPGV11:131-140}.
In this method, connected components are first computed locally in each process. 
Afterwards, in order to identify components that cross the subdomain boundaries, all labels that occur at the boundaries are sent to the respective neighbor processes.
These processes then identify if the boundary components constitute any of their components.
If this is the case, a union structure is used to record the identity.
Afterwards, there is an all-to-all exchange of the unions which results in a globally unique assignment of labels to each component.

Additionally, the transfer of particle labels to their respective seed points must be handled explicitly.
That is, for each particle we store its ID within the original subdomain, and the process ID responsible for that subdomain.
This information is then used to transfer the labels to the correct processes and to save the label at the correct position in the label array.

\section{Summary and conclusion}


\label{sec:summary}

In this paper, we re-compute the high energetic binary droplet collision with three successively refined grid resolution with the finest mesh consisted of as much as 1 billion cells. We find that refining the resolution does not lead to a better agreement with the experiments. In the nonlinear phase in terms of the rim instability, although the PR instability theory is not employable, the instability pattern that shares the characteristic of the PR instability are observed.

Until now, we are not able to reproduce simulation results at Weber number $We=803.2$, which are in good agreement with corresponding experiment. However, if we consider the development of the rim instability as an signal amplification system that magnifies an input signal, which is a white noise, and yields an output signal, which is the 
entirety of the ejected secondary droplets, it is clear that the ejection of secondary droplets is dependent on the strength of the initial white noise. Since on one hand the initial white noise is not known in the experiment, on the other Hand, the systematic noise in the simulation is not avoidable, an agreement between simulations and experiments in terms of the size distribution and time of ejection of secondary droplets is not always expectable.

Furthermore, the signal amplification system, i.e. the development of the rim instability, can be roughly subdivided into three sequential connected subsystems corresponding to three phases in terms of the geometrical characteristic of the rim, see also the schematic illustration in Figure \ref{fig:system_division}: 

\begin{itemize}
 \item Initial phase (Subsystem I):
  the period between the contact of the droplets and the emerging of the toroidal rim. This subsystem magnifies an initial signal, which outcome is then transmitted to subsystem II. 
 \item Linear phase (Subsystem II): In this period, a toroidal rim forms and gradually grows to fingers. This period ends when a neck forms on the finger. The signals in the initial and linear phase can be characterized by the FFT analysis presented in the previous section.
 \item Nonlinear phase (Subsystem III): This period begins with the formation of the neck and lasts to infinite. The signal magnified by the subsystem II is further magnified, resulting in fingering and possible detachment of secondary droplets. In this phase, the outcome can be characterized by the number of fingers and the size spectrum of the secondary droplets, although a clear quantification is until now not possible due to the complex geometry and topology of the collision complex in this phase.
\end{itemize}

We then can conclude in a more precious way that the PR-instability dominates the rim instability growth in the linear phase. The lamella plays probably a more important roll in the initial phase as the ratio between the film thickness and the rim radius is relatively large. The role of the lamella thickness in the initial unstable phase should be studied in the future.

\begin{figure}
\includegraphics[width=1\textwidth]{figures/system_division.png}
\caption{Subdivision of the collision process into  three sequential connected signal amplification system (indicated as I, II and III) based on the geometrical characteristic of the rim.}
\label{fig:system_division}
\end{figure}



% \begin{thebibliography}{00}
% 
% %% \bibitem{label}
% %% Text of bibliographic item
% 
% \bibitem{}
% 
% \end{thebibliography}

\section*{References}
\bibliography{highresolution}{}
\bibliographystyle{plain}

\end{document}
\endinput
%%
%% End of file `elsarticle-template-num.tex'.
